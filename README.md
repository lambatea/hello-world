#README_FILE

In this project the following technologies used to run the hello world java application

1) Gitlab -> Source Code repository

https://gitlab.com/lambatea/hello-world

git@gitlab.com:lambatea/hello-world.git
https://gitlab.com/lambatea/hello-world.git

Here is the simple java application:</br>

<br>#######################################</br>
<br>public class HelloWorld {</br>
   <br> public static void main(String[] args) {</br>
       <br> System.out.println("Hello World");</br>
  <br>  }</br>
<br>}</br>
<br>########################################</br>

<br>2) Created simple YAML file for running this Hello World application through Gitlab CI/CD pipeline:</br>

<br>https://gitlab.com/lambatea/hello-world/pipelines</br>
<br>https://gitlab.com/lambatea/hello-world/-/jobs</br>

<br>########################################</br>

image: java:latest</br>

<br>stages:</br>
  <br>- build</br>
  <br>- execute</br>

<br>build:</br>
  <br>stage: build</br>
  <br>script: /usr/lib/jvm/java-8-openjdk-amd64/bin/javac HelloWorld.java</br>
  <br>artifacts:</br>
    <br>paths:</br>
     <br>- HelloWorld.*</br>

<br>execute:</br>
  <br>stage: execute</br>
  <br>script: /usr/lib/jvm/java-8-openjdk-amd64/bin/java HelloWorld</br>
<br>##############################################</br>

<br>3) Also used Jenkins job to kickoff the build to run the hello world application:</br>

Jenkins Portal:
http://18.222.201.32:8080/login?from=%2F


Jenkins Job:
http://18.222.201.32:8080/job/Hello%20World/

